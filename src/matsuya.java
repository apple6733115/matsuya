import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class matsuya {
    private JButton curryButton;
    private JButton negiButton;
    private JButton gyozaButton;
    private JButton appleButton;
    private JButton sobaButton;
    private JButton grapeButton;
    private JButton checkOutButton;
    private JPanel root;
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JPanel orderedItemsList;
    private JButton textbottan;
    int sum = 0 ;

    public static void main(String[] args) {
        JFrame frame = new JFrame("matsuya");
        frame.setContentPane(new matsuya().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void bottan (String foodname,int price){
        int confirmation = JOptionPane.showConfirmDialog(null ,"Would you like to order "+foodname+" ?" ,
                "Order Confirmation",JOptionPane.YES_NO_OPTION );

        if(confirmation == 0 ){

            JOptionPane.showMessageDialog(null , "Order for " + foodname + " received .");

            String currentText = textArea1.getText();
            textArea1.setText(currentText + foodname + "      " + price + " yen" + "\n");

            sum += price ;
            textArea2.setText(sum + "yen");



        }
    }

    public matsuya() {

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                bottan("gyoza",300);

            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bottan("soba" ,400);
            }
        });
        negiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bottan("negi",100);

            }
        });
        appleButton.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {

                bottan("apple",1000);


            }
        });
        grapeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bottan("grape",500);

            }
        });
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bottan("curry",700);

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null ,"Would you like to check out ?" ,
                        "Order Confirmation",JOptionPane.YES_NO_OPTION );

                if(confirmation == 0 ) {

                    JOptionPane.showMessageDialog(null, "Thank you for using  !!");
                    textArea1.setText("");
                    textArea2.setText("");


                }




            }
        });
    }
}
